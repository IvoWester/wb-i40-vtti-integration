from lib.data_service import DataService
from lib.helper_functions import *
from alfa_sdk import AlgorithmClient
from static.config import settings


def run(name):
    """
    This function is triggered when a file is uploaded on ALFA with the name specified in the config file. This function
    fetches a parsed csv from ALFA, transforms it to a pandas DataFrame and runs the algorithm for all assets. The
    algorithm results are then used to calculate KPIs and update the QuickSight dashboard.
    """
    data_client = DataService()
    algo_client = AlgorithmClient()

    print(f'Fetching file {name} from ALFA.')
    data = data_client.fetch_and_parse_file(file_name=name)

    if (
            'travel_time_open' in data.columns
            and 'travel_time_close' in data.columns
    ):
        problem = {
            "tag": data['asset'].iloc[0],
            "data": name
        }

        print(f"Trigger algorithm for {data['asset'].iloc[0]}")
        algorithm_results = algo_client.invoke(
            algorithm_id=settings.algorithm_configurations.algorithm_id,
            environment=settings.algorithm_configurations.algorithm_environment,
            problem=problem
        )
    else:
        algorithm_results = []
        print('No results from algorithm')

    return algorithm_results