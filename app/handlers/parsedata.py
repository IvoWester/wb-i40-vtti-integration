from lib.data_service import DataService
from static.config import settings
import pandas as pd
import re

VALVE_PROPERTIES = [
    "Size",
    "Type of valve",
    "Type of actuator",
    "Gearbox",
    "Torque rating actuator (Nm)",
    "Torque rating valve (Nm)",
]

def run(name):

    data_client = DataService()
    print('Fetch and unzip files from ALFA.')
    df_all = data_client.fetch_multiple_files(file_name=name)

    print('Combining the data of all valves.')
    df_dict = set_valve_travel_times(df_all)

    print('Upload DataFrames to ALFA.')
    upload_data(data_client, df_dict)

    return print('Successfully uploaded all data to ALFA.')

def find_closest_torque_rating(x, options=[40, 85, 168, 255]):
    o = pd.Series(options)
    return o.iloc[(o - x).abs().argsort()].iloc[0]

def set_properties(properties):
    """
    This function transforms the MOVs properties file on ALFA to usable properties to group the valves.

    :param properties: pandas DataFrame
        The DataFrame should have the properties of all valves.
    :return: properties: pandas DataFrame
        Transformed to a usable format.
    """

    valid_cols = [col for col in properties.columns if col.find('Unnamed: ') != 0]
    properties = properties[valid_cols].copy()
    properties = properties.iloc[:-1].copy()
    properties.Size = properties.Size.str.extract('(\d{1,2})')
    properties.Size = properties.Size.astype('category')
    properties[['Type of valve', 'Type of actuator', 'Gearbox']] = \
        properties[['Type of valve', 'Type of actuator', 'Gearbox']].astype('category')

    # Correct 'Torque rating valve (Nm)' column
    correct_format_rating = (properties.loc[
                                 properties['Torque rating valve (Nm)'].str.isnumeric() == False,
                                 'Torque rating valve (Nm)']
                             .str.extract('(\d{3,4})')
                             )

    properties.loc[
        properties['Torque rating valve (Nm)'].str.isnumeric() == False,
        'Torque rating valve (Nm)'] = correct_format_rating

    properties['Torque rating valve (Nm)'] = pd.to_numeric(properties['Torque rating valve (Nm)'])

    properties['Torque rating valve (Nm)'] = properties['Torque rating valve (Nm)'].apply(
        find_closest_torque_rating
    )
    properties["cat"] = properties[VALVE_PROPERTIES].apply(
        lambda row: "_".join(row.values.astype(str)), axis=1
    )

    return properties

def set_valve_travel_times(df_all):
    """
    Computes the travel times for all different valves.

    :param df_all: pandas DataFrame.
        This DataFrame should be a combined DataFrame of the data of all individual valves.
    :return: dictionary of pandas DataFrames.
        The outcome is a dictionary of pandas DataFrames.
    """

    r = re.compile('[0-9]{4}[A-D]?')

    all_found = []
    for col in df_all.columns:
        valve_id = r.findall(col)
        if len(valve_id) != 0:
            all_found.extend(valve_id)

    all_unique = list(set(all_found))

    # %% Per unique valve, try to isolate travel time and torque readings. Check if valve size is known.

    df_dict = {}
    for idx, valve in enumerate(all_unique):
        try:
            valve_df = df_all[[f'MOV{valve}T.PV', f'TMOV{valve}O.PV', f'TMOV{valve}C.PV']].copy()
        except KeyError:
            valve_df = df_all[[f'MOV{valve[:-1]}T.PV', f'TMOV{valve[:-1]}O.PV', f'TMOV{valve}.PV']].copy()
            valve = valve[:-1]

        valve_df.columns = ['Torque', 'travel_time_open', 'travel_time_close']
        valve_df = valve_df.groupby(level=0).mean()
        if (valve_df.Torque.sum()) == 0 or (valve_df.Torque.std() == 0):
            valve_df.drop(columns=['Torque'], inplace=True)
        if (valve_df.travel_time_open.sum()) == 0 or (valve_df.travel_time_open.std() == 0):
            valve_df.drop(columns=['travel_time_open'], inplace=True)
        if (valve_df.travel_time_close.sum()) == 0 or (valve_df.travel_time_close.std() == 0):
            valve_df.drop(columns=['travel_time_close'], inplace=True)

        if valve_df.shape[1] == 0:
            continue

        #     try:
        #         size = int(properties.loc[f'MOV-{valve}', 'Size'])
        #     except:
        #         print(f'Valve {valve} has no size, is dropped.')
        #         continue

        #     valve_df['Size'] = size
        df_dict.update({valve: valve_df})

    # For some reason, 5311A, 8511C has it's travel_time_close stored in a deviating format. It appears that
    # the counter is reset directly after the movement, which is not supposed to happen. Only one
    # movement each, so drop for sake of workability.
    if '5311A' in df_dict.keys():
        del df_dict['5311A']
    if '8511C' in df_dict.keys():
        del df_dict['8511C']

    return df_dict

def upload_data(data_client, df_dict):
    """
    This function parses the dictionary of DataFrames into multiple pandas DataFrame.
    :param df_dict: dictionary of pandas DataFrames
        The dictionary to parse.
    :return: pandas DataFrame
        Returns a pandas DataFrame that is a parsed DataFrame from the dictionary of DataFrames.
    """
    for asset_id, ddata in df_dict.items():
        print(f'start {asset_id}')
        new_data = ddata.copy()
        new_data.reset_index(drop=False, inplace=True)
        new_data.rename(columns={'timestamp': 'time'}, inplace=True)
        new_data['asset'] = asset_id
        new_data.set_index('time', drop=True, inplace=True)
        data_client.upload(new_data, settings.files_used.file_regex_parsed_data + '_' + asset_id + '.csv')
        print(f'Uploaded asset {asset_id}')
