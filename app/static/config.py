"""
Settings used in the wb-i40-utils package.
"""
from dotmap import DotMap

settings = DotMap({
    "host": {
        # "meta": "https://meta-u.widgetbrain.io/api/",
        # "baas": "https://baas-u.widgetbrain.io/api/",
        "data": "https://data.widgetbrain.io/api/"
    },
    "team_id": "ee80d7b7-3d9b-480e-9386-452c18c83917",
    "requests": {
        "retry": 0,  # 4,
        "delay": 1,  # 10,
        "base": 3
    },
    "algorithm_configurations": {
                                "algorithm_id": "a08ce090-d3f6-4d62-bd1c-3dbb42d57f26",
                                "algorithm_environment": "production"
                                 },
    "files_used": {
        "file_regex_parsed_data": "parsed_data",
        "file_regex_new_data": "data_week",
        "properties_file": "MOVs properties.csv"
    }
    ,

})
