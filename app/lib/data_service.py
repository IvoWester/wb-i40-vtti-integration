"""
Contains class for handling communication with meta units of the i40 Anomaly Detection algorithm.
"""
import io
import pandas as pd

from alfa_sdk.common.session import Session
from alfa_sdk import DataClient
from static.config import settings
from wb_python_utils import request
from zipfile import ZipFile
#

class DataService:
    """Handles communication with data service"""

    def __init__(self):
        self._session = Session().http_session
        self._host = settings.host.data
        self._data_client = DataClient()


    def get_file_list(self, order):
        """
        Returns list of files for the team.
        """
        return self._data_client.list_data_files(order=order)


    def upload(self, data_frame, file_name, description=""):
        """Uploads pandas dataframe as zip to dataservice.

        Parameters
        ----------
        data_frame : pandas dataframe
            should contain 'time' column.
        file_name : str
            name of file in data service
        description : str, optional
            detailed description of file, by default ""

        Returns
        -------
        data service object or error
        """
        return request.execute(
            url=f"{self._host}Storages/upload",
            method="POST",
            params={
                "teamId": settings.team_id,
                "name": file_name,
                "description": description
            },
            files={"file": data_frame.to_csv()},
            session=self._session,
            **settings.requests.toDict()
        )


    def fetch_and_parse_file(self, file_name=None, download_url=None, unzip=False):
        """Fetches file from data service and loads as pandas dataframe.

        Parameters
        ----------
        file_name : str, optional
            file_name of file in data service, by default None
        file_id : str, optional
            id of file object in data serice, by default None
        download_url : str, optional
            output of storages/download/{id} endpoint that is a direct download url, by default None
        unzip : bool, optional
            If true, then it expects a zipped pd dataframe, and will add compression argument in
            parsing. If false it expects a csv file.

        Returns
        -------
        pandas DataFrame

        Raises
        ------
        KeyError
            if none of the arguments are specified
        """
        if file_name is not None:
            return self._load_with_file_name(file_name, unzip)
        if download_url is not None:
            return self._load_with_url(download_url, unzip)

        raise KeyError("Specify either <file_name>, <file_id> or <download_url>")

    def fetch_and_parse_file_xlsb(self, download_url=None):
        """Fetches xlsx file from data service and loads as pandas dataframe.

                Parameters
                ----------
                download_url : str, optional
                    output of storages/download/{id} endpoint that is a direct download url, by default None

                Returns
                -------
                pandas DataFrame

                Raises
                ------
                KeyError
                    if none of the arguments are specified
                """
        if download_url is not None:
            return self._load_with_url(download_url)

        raise KeyError("Specify <download_url>")

    def _load_with_file_name(self, file_name, unzip):
        """
        Loads file from dataclient from file_name and returns pandas DataFrame.
        """
        data_files = self._data_client.list_data_files(file_name)

        if len(data_files) == 0:
            raise ValueError(f'File {file_name} not found')

        data_file = data_files[0]
        file_content = self._data_client.fetch_data_file(data_file["id"])

        if unzip:
            return pd.read_csv(io.BytesIO(file_content), compression="zip")

        return pd.read_csv(io.BytesIO(file_content), index_col=0)


    def _load_with_url(self, download_url, unzip):
        """
        Loads file from download_url and returns pandas DataFrame.
        """
        if unzip:
            return pd.read_csv(download_url, compression="zip")

        return pd.read_csv(download_url, index_col=0)

    def fetch_multiple_files(self, file_name=None):
        """Fetches multiple files from data service and returns a merged pandas DataFrame.

        Parameters
        ----------
        file_name : str
            file_name of file in data service, by default None

        Returns
        -------
        pandas DataFrame
        """

        data_files = self._data_client.list_data_files(file_name)

        if len(data_files) == 0:
            raise ValueError(f'File {file_name} not found')

        data_file = data_files[0]
        file_content = self._data_client.fetch_data_file(data_file["id"])

        zip_file = ZipFile(io.BytesIO(file_content))
        all_periods = []
        all_valves = []

        for text_file in zip_file.infolist():
            try:
                df = pd.read_csv(
                    zip_file.open(text_file.filename),
                    usecols=[i for i in range(2, 11)],
                    skiprows=13,
                    parse_dates=[[0, 1]],
                    dayfirst=True
                )
                df.drop(columns=df.columns[1], inplace=True)

                new_cols = pd.read_csv(
                    zip_file.open(text_file.filename),
                    usecols=[i for i in range(5, 11)],
                    skiprows=4,
                    nrows=1
                ).columns.to_list()

                new_cols.insert(0, 'timestamp')
                df.columns = new_cols

                df.set_index('timestamp', drop=True, inplace=True)

                all_valves.append(df)
            except:
                print(f'{text_file.filename} is excluded due to exception.')

        df_all = pd.concat(all_valves, axis=1)
        all_periods.append(df_all)

        df_all = pd.concat(all_periods, axis=0)
        valid_cols = [col for col in df_all.columns if col.find('Unnamed: ') != 0]
        df_all = df_all[valid_cols]

        # dfs = {text_file.filename: pd.read_csv(zip_file.open(text_file.filename))
        #        for text_file in zip_file.infolist()
        #        if text_file.filename.endswith('.csv')}

        return df_all