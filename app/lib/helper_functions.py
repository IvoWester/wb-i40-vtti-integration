import os
import pandas as pd
from static.config import settings


def merge_results(results, movements_df, cat, valve):
    merged_results = pd.merge(
        results['travel_time_open'],
        results['travel_time_close'],
        left_index=True, right_index=True
    )
    merged_results.rename(columns={
        'travel_time_open': "score_open",
        'travel_time_close': "score_close"
    }, inplace=True)
    new_ = pd.merge(
        merged_results,
        movements_df,
        left_index=True, right_index=True
    )


    new_.loc[new_.travel_time_open.isna(), "stroke_type"] = "close"
    new_.loc[~new_.travel_time_open.isna(), "stroke_type"] = "open"
    new_.loc[new_.score_open.isna(), "score"] = new_.score_close
    new_.loc[new_.score_close.isna(), "score"] = new_.score_open
    new_.loc[new_.travel_time_open.isna(), "duration"] = new_.travel_time_close
    new_.loc[new_.travel_time_close.isna(), "duration"] = new_.travel_time_open
    new_["category"] = cat
    new_["valve"] = valve

    new_["scaled_score"] = round((4 - new_["score"].abs())*25, 2)
    new_["anomaly"] = 0
    new_.loc[new_.scaled_score < 50, "anomaly"] = 1

    return_df = (
        new_.reset_index(drop=False)
        .loc[:, ["category", "valve", "stroke_type", "time", "duration", "score", "scaled_score", "anomaly"]]
    )
    return_df.rename(columns={"time":"start_time"}, inplace=True)
    return return_df


def get_data_per_category(categories_with_valves, data_frame):
    data_for_categories = {}
    for category, valves in categories_with_valves.items():
        merged_data_frame = pd.DataFrame([])
        for valve in valves:
            print(valve)
            if valve in data_frame.keys():
                data_to_add = data_frame[valve].copy()
                data_to_add["asset"] = valve
                merged_data_frame = pd.concat([merged_data_frame, data_to_add])

        data_for_categories[category] = merged_data_frame.copy()

    return data_for_categories


def complete_preprocess_config(data_frame):
    if "asset" in data_frame.columns:
        all_movements = None
        for valve, ddata_frame in data_frame.groupby("asset"):
            if all_movements is None:
                all_movements = preprocessing_travel_time(ddata_frame)
            else:
                all_movements = pd.concat(
                    [all_movements, preprocessing_travel_time(ddata_frame)]
                )
        return all_movements

    return preprocessing_travel_time(data_frame)


def preprocessing_travel_time(data_frame):

    open_stroke_list = []
    close_stroke_list = []
    closed_df = pd.DataFrame([])
    open_df = pd.DataFrame([])
    if (
        "travel_time_close" in data_frame.columns
        and len(data_frame.travel_time_close.dropna()) > 0
    ):
        close_starts = data_frame.loc[data_frame.travel_time_close == 1, :].index

        for start in close_starts:
            sub_data_frame = data_frame.loc[start:, "travel_time_close"]
            end = sub_data_frame.diff().eq(0).idxmax()
            travel_time = (end - start).total_seconds()

            close_stroke_list.append((start, travel_time))

        closed_df = pd.DataFrame(close_stroke_list)
        closed_df.columns = ["time", "travel_time_close"]

    if (
        "travel_time_open" in data_frame.columns
        and len(data_frame.travel_time_open.dropna()) > 0
    ):
        open_starts = data_frame.loc[data_frame.travel_time_open == 1, :].index

        for start in open_starts:
            sub_data_frame = data_frame.loc[start:, "travel_time_open"]
            end = sub_data_frame.diff().eq(0).idxmax()
            travel_time = (end - start).total_seconds()

            open_stroke_list.append((start, travel_time))

        open_df = pd.DataFrame(open_stroke_list)
        open_df.columns = ["time", "travel_time_open"]

    combined_movements = pd.concat([closed_df, open_df], ignore_index=True)

    if combined_movements.shape == (0, 0):
        return combined_movements

    return combined_movements.set_index("time")


def identify_movements(df_dict, properties, valves):
    open_plot_list = []
    close_plot_list = []
    for valve, df in df_dict.items():
        if valve not in valves:
            continue

        size = properties.at[f"MOV-{valve}", "Size"]
        vtype = properties.at[f"MOV-{valve}", "Type of valve"]
        gearbox = properties.at[f"MOV-{valve}", "Gearbox"]
        tra = properties.at[f"MOV-{valve}", "Torque rating actuator (Nm)"]
        trv = properties.at[f"MOV-{valve}", "Torque rating valve (Nm)"]
        # config_id = combi_count.at[(size, vtype, gearbox, tra, trv), 'combi_id']

        try:

            close_starts = df.loc[df.travel_time_close == 1, :].index
            close_ends = []
            for start in close_starts:

                df_ = df.loc[start:, "travel_time_close"]
                close_end = df_.diff().eq(0).idxmax()
                travel_time = (close_end - start).total_seconds()

                if "Torque" in df.columns:
                    close_plot_list.append(
                        (
                            size,
                            vtype,
                            gearbox,
                            tra,
                            trv,
                            travel_time,
                            df.loc[start:close_end, "Torque"].sum() / travel_time,
                            df.loc[start:close_end, "Torque"].max(),
                            valve,
                            start,
                            close_end,
                        )
                    )
                else:
                    close_plot_list.append(
                        (
                            size,
                            vtype,
                            gearbox,
                            tra,
                            trv,
                            travel_time,
                            pd.NA,
                            pd.NA,
                            valve,
                            start,
                            close_end,
                        )
                    )

        except AttributeError as e:
            print(valve, f"Error: {e}")
            pass

        try:

            open_starts = df.loc[df.travel_time_open == 1, :].index
            open_ends = []
            for start in open_starts:

                df_ = df.loc[start:, "travel_time_open"]
                open_end = df_.diff().eq(0).idxmax()
                travel_time = (open_end - start).total_seconds()

                if "Torque" in df.columns:
                    open_plot_list.append(
                        (
                            size,
                            vtype,
                            gearbox,
                            tra,
                            trv,
                            travel_time,
                            df.loc[start:open_end, "Torque"].sum() / travel_time,
                            df.loc[start:close_end, "Torque"].max(),
                            valve,
                            start,
                            open_end,
                        )
                    )

                else:
                    open_plot_list.append(
                        (
                            size,
                            vtype,
                            gearbox,
                            tra,
                            trv,
                            travel_time,
                            pd.NA,
                            pd.NA,
                            valve,
                            start,
                            open_end,
                        )
                    )

        except AttributeError as e:
            print(valve, f"Error: {e}")
            continue

    c = pd.DataFrame(close_plot_list)
    c.columns = [
        "Diameter",
        "Type of valve",
        "Gearbox",
        "Torque rating actuator (Nm)",
        "Torque rating valve (Nm)",
        "Time",
        "Torque sum",
        "Torque max",
        "valve",
        "start",
        "end",
    ]

    c["Action"] = "Close"

    o = pd.DataFrame(open_plot_list)
    o.columns = [
        "Diameter",
        "Type of valve",
        "Gearbox",
        "Torque rating actuator (Nm)",
        "Torque rating valve (Nm)",
        "Time",
        "Torque sum",
        "Torque max",
        "valve",
        "start",
        "end",
    ]

    o["Action"] = "Open"

    return pd.concat([c, o], ignore_index=True)


def return_largest_categories_with_valve_ids(properties, n=5):
    largest_n = properties.groupby("cat").size().sort_values(ascending=False).iloc[:5]

    categories_with_valves = {}
    for category, size in largest_n.to_frame().iterrows():
        valves = properties[properties.cat == category].index.to_list()
        categories_with_valves[category] = [valve.split("-")[1] for valve in valves]

    return categories_with_valves


#