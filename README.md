# Widget Brain Industry 4.0 Utilities

This integration has two functions; parsedata and updatequicksight. 

parsedata
Parses the data from the format that VTTI uses to the format that can be used by the algorithm.

updatequicksight
Triggers the algorithm and updates the Quicksight dashboard to contain the new KPIs.

# Changelog
##### 0.1.0 (2021-09-21)
* Initial set up of the integration.